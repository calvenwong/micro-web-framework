const express = require('express');
const Joi = require('joi');
const app = express();
const multer = require('multer');
const fs = require('fs');
const unzipper = require('unzipper');
const path = require('path');

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(express.static('public'));

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'uploads')
    },
    filename: (req, file, callBack) => {
        callBack(null, file.originalname)
    }
  })
  
const upload = multer({ storage: storage })

app.get('/', (req, res) => {
    res.send(`Please attach your picture to this service using POST at /upload`);
})

app.get('/api/uploads/:name', (req, res) => {
    res.download(`./uploads/${req.params.name}`); 
})

app.get('/api/uploads/:folder/:name', (req, res) => {
    res.download(`./uploads/${req.params.folder}/${req.params.name}`);   
})

app.post('/api/upload',upload.single('file'), (req, res) => {
    //const {error} = validatePicture(req.body);
    const file = req.file;
    if (!file) return res.status(400).send('No file');

    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' || 
        file.mimetype === 'image/jpeg' 
        ){
        console.log(file.path);
        res.send(`http://localhost:3000/api/${file.destination}/${file.filename}`);
    }
    else if(file.mimetype === 'application/zip') {
        links = [];

        fs.createReadStream(file.path.toString())
        .pipe(unzipper.Extract({path: "uploads/"}));
        
        let directoryPath = file.path.substring(0, file.path.length -4);

        fs.readdir(directoryPath, function (err, files) {
            
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            } 
            //search each file in unzipped folder and generate link
            let temp = file.path.substring(0, file.path.length - 4).replace(/\\/g,"/");
            files.forEach(function (file) {
                links.push(`http://localhost:3000/api/${temp}/${file}`);
            });
            console.log(file);
            res.send(links);
        });
    }
    else{
        console.log(file.path);
        fs.unlinkSync(file.path);
        res.send('File is Invalid');
    }

        
})


function validatePicture(file){
    const schema = {
        name: Joi.string().min(3).required()
    }

    return Joi.validate(file, schema);
}


//PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));